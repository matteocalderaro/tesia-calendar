let ScheduleDetails = ()=>{

  let mondayIndex = 0
  
  let date = new Date()
  
  currYear = date.getFullYear()
  currMonth = date.getMonth()
  
  let calendar = []
  let api = []
  let reduced = []
  
  /////////////////////////
  // CREATE CALENDAR FE
  ////////////////////
  
  const createCalendar = () => {
    for (let i = 0; i < 1000; i++) {
      calendar.push(new Date(currYear,currMonth - 4 ,i +2 ).toISOString().slice(0, 10))
    }
  }
  createCalendar()
  
  /////////////////////
  // FETCH DATA
  /////////////////
  
  fetch('/event.json')
    .then(response=>response.json())
    .then(
      dates => {
        dates.forEach(data=> api.push(data)) 
      })
    .finally(()=>{
  
  ////////////////////////////////////////////////
  // REDUCED ARRAY - MONDAY INDEX - WEEK ARRAY 
  ///////////////////////////////////////////
  
    // reduced array
    function findCommonElement(calendar, api) {
  
      for(let i = 0; i < calendar.length; i++) {
        for(let j = 0; j < api?.length; j++) {
              
            if(calendar[i] === api[j]?.day) {
                reduced.push(api[j])
            }
          }
  
        if(!reduced.find(b=>b.day === calendar[i])) reduced.push({
          day: calendar[i],
          booked: false}
        )  
      }
    }
    findCommonElement(calendar, api)
    
  
    // find currentDay index
    let today = new Date().toLocaleString('en-GB', { timeZone: "Europe/London" }).slice(0, 10).split('/').reverse().join('-')
    //let today = new Date().toISOString().slice(0, 10)
    console.log('today',today);
  
    //let today = "2023-05-01"
    let currentDay = reduced.find(data => data.day === today)
    let currentDayIndex = reduced.findIndex(obj=> obj.day === currentDay.day)
    // find monday index
    let currentDayName = new Date(currentDay.day).toString().split(' ')[0].toLocaleLowerCase()
    switch (currentDayName) {
      case 'sun' : mondayIndex = currentDayIndex -6
      break
      case 'sat' : mondayIndex = currentDayIndex -5
      break
      case 'fri' : mondayIndex = currentDayIndex -4
      break
      case 'thu' : mondayIndex = currentDayIndex -3
      break
      case 'wed' : mondayIndex = currentDayIndex -2
      break
      case 'tue' : mondayIndex = currentDayIndex -1
      break
      case 'mon' : mondayIndex = currentDayIndex
      break
      default : return
    }
  
    //define week array
    let weekArray = [...reduced].splice(mondayIndex,7)
    createDay(weekArray,currentDay)
    // render monday event
    createEvent(currentDay.day,currentDay.event)
  
    console.log('CALENDAR',calendar)
    console.log('API',api)
    console.log('reduced',reduced);
  })
  
  ///////////////////////////////
  // BUTTONS - event listener
  //////////////////////////////
  
  // button left
  let container = document.querySelector('.container')
  let btnLeft = document.createElement('button')
  btnLeft.classList.add('btn')
  btnLeft.classList.add('btn-left')
  btnLeft.innerHTML=`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z"/></svg>
  `
  btnLeft.addEventListener('click',()=>{
    if(mondayIndex > 0) mondayIndex -= 7
    createWeekArray()
  })
  container.appendChild(btnLeft)
  
  // button right
  let btnRight = document.createElement('button')
  btnRight.classList.add('btn')
  btnRight.classList.add('btn-right')
  btnRight.innerHTML=`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z"/></svg>
  `
  btnRight.addEventListener('click',()=>{
    mondayIndex += 7
    createWeekArray()
  })
  container.appendChild(btnRight)
  
  // button reload
  let btnReload = document.createElement('button')
  btnReload.classList.add('btn-reload')
  btnReload.innerHTML='TODAY'
  
  btnReload.addEventListener('click',()=>window.location.reload())
  container.appendChild(btnReload)
  
  // create week
  function createWeekArray(){
    let weekArray = [...reduced].splice(mondayIndex, 7)
  
    if(weekArray.length != 0 && weekArray.length===7){
      createDay(weekArray)
      // render monday event
      createEvent(weekArray[0].day,weekArray[0].event)
    } 
    //logMondayIndex()
  }
  
  /////////////////////////////
  // DAY - create element
  //////////////////////////
  
    function createDay(weekArray,currentDay){
        // week__wrapper
        let weekWrapper = document.querySelector(".week__wrapper")
        weekWrapper.innerHTML =""
    
        // week__label
        let weekLabel = document.createElement("div")
        weekLabel.classList.add('week__label')
        weekLabel.innerHTML = `
          ${weekArray[0].day.split("-").reverse().join("/")} - ${weekArray[6].day.split("-").reverse().join("/")}`
        weekWrapper.appendChild(weekLabel)
    
        // day__container
        weekArray.forEach((day)=>{
          let dayContainer = createDayInfo(day)
          dayContainer.classList.add('day__container')
          if(day === currentDay){
            dayContainer.classList.add('selected')
          }
          weekWrapper.appendChild(dayContainer)
        })
        
        // day__info
        function createDayInfo({day,event}) {
          //console.log(day,event);
    
          let dayElement = document.createElement("div")
              dayElement.innerHTML = `
                <div class="day__info" >
                  <div class="day__info--dayName">
                    ${new Date(day).toString().split(' ')[0]}
                  </div>
                  <div class="day__info--fullDate">${day.split("-").reverse().join("/")}</div>
                </div>
                <div class="day__bookingBar" style="background-color : ${event ? '#ef4444' : 'transparent'}">
                </div>
              `
              dayElement.addEventListener('click',()=>createEvent(day,event))
              dayElement.style.cursor = 'pointer'
          return dayElement
        }
    
        dayEventListener()
        //logMondayIndex()
    }
  
  
  //////////////////////////////
  // HOURS - create element
  ///////////////////////////
  
  function createEvent(day,event){
    //console.log(day,event);
    let hoursWrapper = document.querySelector(".hours__wrapper")
    hoursWrapper.innerHTML =""
    
    let hourElement = document.createElement("div")
    hourElement.classList.add('hours__progressBar')
    hourElement.innerHTML=`
      <div style="color: #777;font-size:1.5rem">${day.split("-").reverse().join("/")}</div>
      <div style="background-color : ${event  ? '#ef4444' : '#777'}">
        ${event ? event :'nessun evento' }
      </div>`
    hoursWrapper.appendChild(hourElement)
  }
  
  ///////////////////////////
  // TOGGLE DAY SELECTED
  ///////////////////////
  
  function dayEventListener(){
    let weekArray = Array.from(document.querySelectorAll('.day__container'))
    weekArray.forEach(day=> day.addEventListener('click',()=>{
  
        day.classList.toggle('selected')
        
        let siblings = getSiblings(day)
  
        siblings.map(e => {
          e.classList.remove('selected')
        })
      })
    )
  }
  
  function getSiblings(e) {
    // for collecting siblings
    let siblings = []; 
    // if no parent, return no sibling
    if(!e.parentNode) {
        return siblings;
    }
    // first child of the parent node
    let sibling  = e.parentNode.children[1];
    // collecting siblings
    while (sibling) {
        if (sibling.nodeType === 1 && sibling !== e) {
            siblings.push(sibling);
        }
        sibling = sibling.nextSibling;
    }
    //console.log(siblings);
    return siblings;
  };
  
  //////////////////////////////
  // CONSOLE LOG MONDAY INDEX
  ///////////////////////
  
  function logMondayIndex(){
    let mondayContainer = document.querySelector('.mondayContainer')
    mondayContainer.innerHTML=''
    let consoleLog = document.createElement('div')
    consoleLog.classList.add('consoleLog')
    consoleLog.innerHTML=`Monday Index: ${mondayIndex}`
    mondayContainer.appendChild(consoleLog)
  }
}


